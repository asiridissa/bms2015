﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using BMS.Data;

namespace BMS.Web.Controllers
{
    public class VehicleStockController : Controller
    {
        private BMSEntities db = new BMSEntities();

        public ActionResult Index()
        {
            ViewData["Lots"] =
                db.Lots.Include("Item.Brand")
                    .Include("Item.Product")
                    .Include("Item.Category")
                    .Select(x => new SelectListItem()
                    {
                        Value = x.ID.ToString(),
                        Text =
                            x.Item.Brand.ShortName + " " + x.Item.Product.ShortName + " " + x.Item.Category.ShortName +
                            " " + x.Item.Weight
                    }).ToList();

            ViewData["Vehicles"] = db.Vehicles.Select(x => new SelectListItem()
            {
                Value = x.ID.ToString(),
                Text = x.No
            }).ToList();

            return View();
        }

        public ActionResult VehicleStocks_Read([DataSourceRequest]DataSourceRequest request)
        {
            IQueryable<VehicleStock> vehiclestocks = db.VehicleStocks;
            DataSourceResult result = vehiclestocks.ToDataSourceResult(request, vehicleStock => new
            {
                ID = vehicleStock.ID,
                LotID = vehicleStock.LotID,
                VehicleID = vehicleStock.VehicleID,
                Cases = vehicleStock.Cases,
                Units = vehicleStock.Units,
                FromDate = vehicleStock.FromDate,
                ThruDate = vehicleStock.ThruDate,
            });

            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult VehicleStocks_Create([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<VehicleStock> vehiclestocks)
        {
            var entities = new List<VehicleStock>();
            if (vehiclestocks != null && ModelState.IsValid)
            {
                foreach (var vehicleStock in vehiclestocks)
                {
                    var entity = new VehicleStock
                    {
                        LotID = vehicleStock.LotID,
                        VehicleID = vehicleStock.VehicleID,
                        Cases = vehicleStock.Cases,
                        Units = vehicleStock.Units,
                        FromDate = vehicleStock.FromDate,
                        ThruDate = vehicleStock.ThruDate,
                    };

                    db.VehicleStocks.Add(entity);
                    entities.Add(entity);
                }
                db.SaveChanges();
            }

            return Json(entities.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult VehicleStocks_Update([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<VehicleStock> vehiclestocks)
        {
            var entities = new List<VehicleStock>();
            if (vehiclestocks != null && ModelState.IsValid)
            {
                foreach (var vehicleStock in vehiclestocks)
                {
                    var entity = new VehicleStock
                    {
                        ID = vehicleStock.ID,
                        LotID = vehicleStock.LotID,
                        VehicleID = vehicleStock.VehicleID,
                        Cases = vehicleStock.Cases,
                        Units = vehicleStock.Units,
                        FromDate = vehicleStock.FromDate,
                        ThruDate = vehicleStock.ThruDate,
                    };

                    entities.Add(entity);
                    db.VehicleStocks.Attach(entity);
                    db.Entry(entity).State = EntityState.Modified;
                }
                db.SaveChanges();
            }

            return Json(entities.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult VehicleStocks_Destroy([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<VehicleStock> vehiclestocks)
        {
            var entities = new List<VehicleStock>();
            if (ModelState.IsValid)
            {
                foreach (var vehicleStock in vehiclestocks)
                {
                    var entity = new VehicleStock
                    {
                        ID = vehicleStock.ID,
                        LotID = vehicleStock.LotID,
                        VehicleID = vehicleStock.VehicleID,
                        Cases = vehicleStock.Cases,
                        Units = vehicleStock.Units,
                        FromDate = vehicleStock.FromDate,
                        ThruDate = vehicleStock.ThruDate,
                    };

                    entities.Add(entity);
                    db.VehicleStocks.Attach(entity);
                    db.VehicleStocks.Remove(entity);
                }
                db.SaveChanges();
            }

            return Json(entities.ToDataSourceResult(request, ModelState));
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
