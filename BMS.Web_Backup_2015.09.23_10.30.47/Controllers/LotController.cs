﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using BMS.Data;

namespace BMS.Web.Controllers
{
    public class LotController : Controller
    {
        private BMSEntities db = new BMSEntities();

        public ActionResult Index()
        {
            ViewData["Items"] = db.Items.Include("Brand").Include("Product").Include("Category").Where(x => x.ThruDate == null).Select(x => new SelectListItem()
            {
                Value = x.ID.ToString(),
                Text = x.Brand.ShortName + " " + x.Product.ShortName + " " + x.Category.ShortName + " " + x.Weight
            }).ToList();
            return View();
        }

        public ActionResult Lots_Read([DataSourceRequest]DataSourceRequest request)
        {
            IQueryable<Lot> lots = db.Lots;
            DataSourceResult result = lots.ToDataSourceResult(request, lot => new
            {
                ID = lot.ID,
                ItemID = lot.ItemID,
                Units = lot.Units,
                FromDate = lot.FromDate,
                ThruDate = lot.ThruDate,
            });

            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Lots_Create([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Lot> lots)
        {
            var entities = new List<Lot>();
            if (lots != null && ModelState.IsValid)
            {
                foreach (var lot in lots)
                {
                    var entity = new Lot
                    {
                        ItemID = lot.ItemID,
                        Units = lot.Units,
                        FromDate = DateTime.UtcNow,
                        ThruDate = lot.ThruDate,
                    };

                    db.Lots.Add(entity);
                    entities.Add(entity);
                }
                db.SaveChanges();
            }

            return Json(entities.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Lots_Update([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Lot> lots)
        {
            var entities = new List<Lot>();
            if (lots != null && ModelState.IsValid)
            {
                foreach (var lot in lots)
                {
                    var entity = new Lot
                    {
                        ID = lot.ID,
                        ItemID = lot.ItemID,
                        Units = lot.Units,
                        FromDate = lot.FromDate,
                        ThruDate = lot.ThruDate,
                    };

                    entities.Add(entity);
                    db.Lots.Attach(entity);
                    db.Entry(entity).State = EntityState.Modified;
                }
                db.SaveChanges();
            }

            return Json(entities.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Lots_Destroy([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Lot> lots)
        {
            var entities = new List<Lot>();
            if (ModelState.IsValid)
            {
                foreach (var lot in lots)
                {
                    var entity = new Lot
                    {
                        ID = lot.ID,
                        ItemID = lot.ItemID,
                        Units = lot.Units,
                        FromDate = lot.FromDate,
                        ThruDate = lot.ThruDate,
                    };

                    entities.Add(entity);
                    db.Lots.Attach(entity);
                    db.Lots.Remove(entity);
                }
                db.SaveChanges();
            }

            return Json(entities.ToDataSourceResult(request, ModelState));
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
