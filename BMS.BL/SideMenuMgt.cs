﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using BMS.Data;

namespace BMS.BL
{
    public class SideMenuMgt
    {
        List<SideMenu> _tree = new List<SideMenu>();

        public List<Data.SideMenu> GetSideMenu()
        {
            using (var c = new BMSEntities())
            {
                _tree = c.SideMenus.Where(x => x.heading == true).OrderBy(x => x.SortId).ToList();
                AppendChildren(_tree);

                return _tree;
            }
        }

        private void AppendChildren(List<SideMenu> nodes)
        {
            foreach (var node in nodes)
            {
                var children = GetMenuChildren(node.Id);

                if (!children.Any()) continue;
                var parent = _tree.Find(x => x.Equals(node));

                if (parent != null) continue;
                _tree.Find(x => x.Equals(node)).submenu = children;

                AppendChildren(children);
            }
        }

        private List<SideMenu> GetMenuChildren(int Id)
        {
            using (var c = new BMSEntities())
            {
                return c.SideMenus.Where(x => x.ParentId == Id).OrderBy(x => x.SortId).ToList();
            }
        }
    }
}
