﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMS.Data;

namespace BMS.BL
{
    public class AreaMgt
    {
        public List<Area> GetArea()
        {
            using (var c= new BMSEntities())
            {
                return c.Areas.ToList();
            }
        } 
    }
}
