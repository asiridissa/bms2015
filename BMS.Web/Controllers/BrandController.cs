﻿﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
﻿using System.Web.Helpers;
﻿using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using BMS.Data;

namespace BMS.Web.Controllers
{
    public class BrandController : Controller
    {
        private BMSEntities db = new BMSEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Brands_Read([DataSourceRequest]DataSourceRequest request)
        {
            IQueryable<Brand> brands = db.Brands;
            DataSourceResult result = brands.ToDataSourceResult(request, brand => new {
                ID = brand.ID,
                ShortName = brand.ShortName,
                FullName = brand.FullName,
                FromDate = brand.FromDate,
                ThruDate = brand.ThruDate,
            });

            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Brands_Create([DataSourceRequest]DataSourceRequest request, Brand brand)
        {
            if (ModelState.IsValid)
            {
                var entity = new Brand
                {
                    ShortName = brand.ShortName,
                    FullName = brand.FullName,
                    FromDate = brand.FromDate,
                    ThruDate = brand.ThruDate,
                };

                db.Brands.Add(entity);
                db.SaveChanges();
                brand.ID = entity.ID;
            }

            return Json(new[] { brand }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Brands_Update([DataSourceRequest]DataSourceRequest request, Brand brand)
        {
            if (ModelState.IsValid)
            {
                var entity = new Brand
                {
                    ID = brand.ID,
                    ShortName = brand.ShortName,
                    FullName = brand.FullName,
                    FromDate = brand.FromDate,
                    ThruDate = brand.ThruDate,
                };

                db.Brands.Attach(entity);
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
            }

            return Json(new[] { brand }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Brands_Destroy([DataSourceRequest]DataSourceRequest request, Brand brand)
        {
            if (ModelState.IsValid)
            {
                var entity = new Brand
                {
                    ID = brand.ID,
                    ShortName = brand.ShortName,
                    FullName = brand.FullName,
                    FromDate = brand.FromDate,
                    ThruDate = brand.ThruDate,
                };

                db.Brands.Attach(entity);
                db.Brands.Remove(entity);
                db.SaveChanges();
            }

            return Json(new[] { brand }.ToDataSourceResult(request, ModelState));
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetBrandJson()
        {
            return Json(db.Brands);
        }

        public IEnumerable<Brand> GetBrand()
        {

            return db.Brands.ToList();
        } 
    }
}
