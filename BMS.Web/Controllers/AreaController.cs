﻿﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using BMS.Data;

namespace BMS.Web.Controllers
{
    public class AreaController : Controller
    {
        private BMSEntities db = new BMSEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Areas_Read([DataSourceRequest]DataSourceRequest request)
        {
            IQueryable<Area> areas = db.Areas;
            DataSourceResult result = areas.ToDataSourceResult(request, area => new {
                ID = area.ID,
                ShortName = area.ShortName,
                FullName = area.FullName,
                FromDate = area.FromDate,
                ThruDate = area.ThruDate,
            });

            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Areas_Create([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Area> areas)
        {
            var entities = new List<Area>();
            if (areas != null && ModelState.IsValid)
            {
                foreach(var area in areas)
                {
                    var entity = new Area
                    {
                            ShortName = area.ShortName,
                            FullName = area.FullName,
                            FromDate = area.FromDate,
                            ThruDate = area.ThruDate,
                    };

                    db.Areas.Add(entity);
                    entities.Add(entity);
                }
                db.SaveChanges();
            }

            return Json(entities.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Areas_Update([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Area> areas)
        {
            var entities = new List<Area>();
            if (areas != null && ModelState.IsValid)
            {
                foreach(var area in areas)
                {
                    var entity = new Area
                    {
                        ID = area.ID,
                        ShortName = area.ShortName,
                        FullName = area.FullName,
                        FromDate = area.FromDate,
                        ThruDate = area.ThruDate,
                    };

                    entities.Add(entity);
                    db.Areas.Attach(entity);
                    db.Entry(entity).State = EntityState.Modified;                        
                }
                db.SaveChanges();
            }

            return Json(entities.ToDataSourceResult(request, ModelState));
        } 

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Areas_Destroy([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Area> areas)
        {
            var entities = new List<Area>();
            if (ModelState.IsValid)
            {
                foreach(var area in areas)
                {
                    var entity = new Area
                    {
                        ID = area.ID,
                        ShortName = area.ShortName,
                        FullName = area.FullName,
                        FromDate = area.FromDate,
                        ThruDate = area.ThruDate,
                    };

                    entities.Add(entity);
                    db.Areas.Attach(entity);
                    db.Areas.Remove(entity);
                }
                db.SaveChanges();
            }

            return Json(entities.ToDataSourceResult(request, ModelState));
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
