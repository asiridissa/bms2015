﻿﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using BMS.Data;

namespace BMS.Web.Controllers
{
    public class CustomerController : Controller
    {
        private BMSEntities db = new BMSEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Customers_Read([DataSourceRequest]DataSourceRequest request)
        {
            IQueryable<Customer> customers = db.Customers;
            DataSourceResult result = customers.ToDataSourceResult(request, customer => new {
                ID = customer.ID,
                ShortName = customer.ShortName,
                FullName = customer.FullName,
                FromDate = customer.FromDate,
                ThruDate = customer.ThruDate,
            });

            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Customers_Create([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Customer> customers)
        {
            var entities = new List<Customer>();
            if (customers != null && ModelState.IsValid)
            {
                foreach(var customer in customers)
                {
                    var entity = new Customer
                    {
                            ShortName = customer.ShortName,
                            FullName = customer.FullName,
                            FromDate = customer.FromDate,
                            ThruDate = customer.ThruDate,
                    };

                    db.Customers.Add(entity);
                    entities.Add(entity);
                }
                db.SaveChanges();
            }

            return Json(entities.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Customers_Update([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Customer> customers)
        {
            var entities = new List<Customer>();
            if (customers != null && ModelState.IsValid)
            {
                foreach(var customer in customers)
                {
                    var entity = new Customer
                    {
                        ID = customer.ID,
                        ShortName = customer.ShortName,
                        FullName = customer.FullName,
                        FromDate = customer.FromDate,
                        ThruDate = customer.ThruDate,
                    };

                    entities.Add(entity);
                    db.Customers.Attach(entity);
                    db.Entry(entity).State = EntityState.Modified;                        
                }
                db.SaveChanges();
            }

            return Json(entities.ToDataSourceResult(request, ModelState));
        } 

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Customers_Destroy([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Customer> customers)
        {
            var entities = new List<Customer>();
            if (ModelState.IsValid)
            {
                foreach(var customer in customers)
                {
                    var entity = new Customer
                    {
                        ID = customer.ID,
                        ShortName = customer.ShortName,
                        FullName = customer.FullName,
                        FromDate = customer.FromDate,
                        ThruDate = customer.ThruDate,
                    };

                    entities.Add(entity);
                    db.Customers.Attach(entity);
                    db.Customers.Remove(entity);
                }
                db.SaveChanges();
            }

            return Json(entities.ToDataSourceResult(request, ModelState));
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
