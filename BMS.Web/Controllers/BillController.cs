﻿﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using BMS.Data;

namespace BMS.Web.Controllers
{
    public class BillController : Controller
    {
        private BMSEntities db = new BMSEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Bill()
        {
            return View();
        }
        public ActionResult BillContent()
        {
            return PartialView();
        }

        public ActionResult BillHeader()
        {
            return PartialView();
        }

        public ActionResult BillLine()
        {
            return PartialView();
        }

        public ActionResult Bills_Read([DataSourceRequest]DataSourceRequest request)
        {
            IQueryable<Bill> bills = db.Bills;
            DataSourceResult result = bills.ToDataSourceResult(request, bill => new {
                ID = bill.ID,
                BillNo = bill.BillNo,
                BillDate = bill.BillDate,
                CreatedUserID = bill.CreatedUserID,
                BillDiscount = bill.BillDiscount,
                FromDate = bill.FromDate,
                FinalizedDate = bill.FinalizedDate,
                ThruDate = bill.ThruDate,
                Varience = bill.Varience,
                BillTotal = bill.BillTotal,
            });

            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Bills_Create([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Bill> bills)
        {
            var entities = new List<Bill>();
            if (bills != null && ModelState.IsValid)
            {
                foreach(var bill in bills)
                {
                    var entity = new Bill
                    {
                            BillNo = bill.BillNo,
                            BillDate = bill.BillDate,
                            CreatedUserID = bill.CreatedUserID,
                            BillDiscount = bill.BillDiscount,
                            FromDate = bill.FromDate,
                            FinalizedDate = bill.FinalizedDate,
                            ThruDate = bill.ThruDate,
                            Varience = bill.Varience,
                            BillTotal = bill.BillTotal,
                    };

                    db.Bills.Add(entity);
                    entities.Add(entity);
                }
                db.SaveChanges();
            }

            return Json(entities.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Bills_Update([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Bill> bills)
        {
            var entities = new List<Bill>();
            if (bills != null && ModelState.IsValid)
            {
                foreach(var bill in bills)
                {
                    var entity = new Bill
                    {
                        ID = bill.ID,
                        BillNo = bill.BillNo,
                        BillDate = bill.BillDate,
                        CreatedUserID = bill.CreatedUserID,
                        BillDiscount = bill.BillDiscount,
                        FromDate = bill.FromDate,
                        FinalizedDate = bill.FinalizedDate,
                        ThruDate = bill.ThruDate,
                        Varience = bill.Varience,
                        BillTotal = bill.BillTotal,
                    };

                    entities.Add(entity);
                    db.Bills.Attach(entity);
                    db.Entry(entity).State = EntityState.Modified;                        
                }
                db.SaveChanges();
            }

            return Json(entities.ToDataSourceResult(request, ModelState));
        } 

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Bills_Destroy([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Bill> bills)
        {
            var entities = new List<Bill>();
            if (ModelState.IsValid)
            {
                foreach(var bill in bills)
                {
                    var entity = new Bill
                    {
                        ID = bill.ID,
                        BillNo = bill.BillNo,
                        BillDate = bill.BillDate,
                        CreatedUserID = bill.CreatedUserID,
                        BillDiscount = bill.BillDiscount,
                        FromDate = bill.FromDate,
                        FinalizedDate = bill.FinalizedDate,
                        ThruDate = bill.ThruDate,
                        Varience = bill.Varience,
                        BillTotal = bill.BillTotal,
                    };

                    entities.Add(entity);
                    db.Bills.Attach(entity);
                    db.Bills.Remove(entity);
                }
                db.SaveChanges();
            }

            return Json(entities.ToDataSourceResult(request, ModelState));
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
