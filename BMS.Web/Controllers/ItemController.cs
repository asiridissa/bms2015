﻿﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using BMS.Data;

namespace BMS.Web.Controllers
{
    public class ItemController : Controller
    {
        private BMSEntities db = new BMSEntities();

        public ActionResult Index()
        {
            ViewData["Brands"] = db.Brands.Where(x => x.ThruDate == null).Select(x => new SelectListItem()
            {
                Value = x.ID.ToString(),
                Text = x.ShortName
            }).ToList();
            ViewData["Categories"] = db.Categories.Where(x => x.ThruDate == null).Select(x => new SelectListItem()
            {
                Value = x.ID.ToString(),
                Text = x.ShortName
            }).ToList();
            ViewData["Products"] = db.Products.Where(x => x.ThruDate == null).Select(x => new SelectListItem()
            {
                Value = x.ID.ToString(),
                Text = x.ShortName
            }).ToList();
            return View();
        }

        public ActionResult Items_Read([DataSourceRequest]DataSourceRequest request)
        {
            IQueryable<Item> items = db.Items;
            DataSourceResult result = items.ToDataSourceResult(request, item => new {
                ID = item.ID,
                BrandID = item.BrandID,
                ProductID = item.ProductID,
                CategoryID = item.CategoryID,
                Weight = item.Weight,
                UnitsPerCase = item.UnitsPerCase,
                UnitPrice = item.UnitPrice,
                BuyingDiscount = item.BuyingDiscount,
                SellingDiscount = item.SellingDiscount,
                FromDate = item.FromDate,
                ThruDate = item.ThruDate,
            });

            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Items_Create([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Item> items)
        {
            var entities = new List<Item>();
            if (items != null && ModelState.IsValid)
            {
                foreach (var item in items)
                {
                    var entity = new Item()
                    {
                        ID = item.ID,
                        BrandID = item.BrandID,
                        ProductID = item.ProductID,
                        CategoryID = item.CategoryID,
                        Weight = item.Weight,
                        UnitsPerCase = item.UnitsPerCase,
                        UnitPrice = item.UnitPrice,
                        BuyingDiscount = item.BuyingDiscount,
                        SellingDiscount = item.SellingDiscount,
                        FromDate = DateTime.UtcNow
                    };

                    db.Items.Add(entity);
                    entities.Add(entity);
                }
                db.SaveChanges();
            }

            return Json(entities.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Items_Update([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Item> items)
        {
            var entities = new List<Item>();
            if (items != null && ModelState.IsValid)
            {
                foreach (var item in items)
                {
                    var entity = new Item()
                    {
                        ID = item.ID,
                        BrandID = item.BrandID,
                        ProductID = item.ProductID,
                        CategoryID = item.CategoryID,
                        Weight = item.Weight,
                        UnitsPerCase = item.UnitsPerCase,
                        UnitPrice = item.UnitPrice,
                        BuyingDiscount = item.BuyingDiscount,
                        SellingDiscount = item.SellingDiscount,
                        FromDate = item.FromDate
                    };

                    entities.Add(entity);
                    db.Items.Attach(entity);
                    db.Entry(entity).State = EntityState.Modified;
                }
                db.SaveChanges();
            }

            return Json(entities.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Items_Destroy([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Item> items)
        {
            var entities = new List<Item>();
            if (ModelState.IsValid)
            {
                foreach (var item in items)
                {
                    var entity = new Item()
                    {
                        ID = item.ID,
                        BrandID = item.BrandID,
                        ProductID = item.ProductID,
                        CategoryID = item.CategoryID,
                        Weight = item.Weight,
                        UnitsPerCase = item.UnitsPerCase,
                        UnitPrice = item.UnitPrice,
                        BuyingDiscount = item.BuyingDiscount,
                        SellingDiscount = item.SellingDiscount,
                    };

                    entities.Add(entity);
                    db.Items.Attach(entity);
                    db.Items.Remove(entity);
                }
                db.SaveChanges();
            }

            return Json(entities.ToDataSourceResult(request, ModelState));

        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetItemJson()
        {
            var model = db.Items.Include("Brand").Include("Product").Include("Category").Where(x => x.ThruDate == null).Select(x => new SelectListItem()
            {
                Value = x.ID.ToString(),
                Text = x.Brand.ShortName + " " + x.Product.ShortName + " " + x.Category.ShortName + " " + x.Weight
            }).ToList();

            return Json(model);
        }

    }
}
