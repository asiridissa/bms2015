﻿﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using BMS.Data;

namespace BMS.Web.Controllers
{
    public class UserController : Controller
    {
        private BMSEntities db = new BMSEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Users_Read([DataSourceRequest]DataSourceRequest request)
        {
            IQueryable<User> users = db.Users;
            DataSourceResult result = users.ToDataSourceResult(request, user => new {
                ID = user.ID,
                Username = user.Username,
                Password = user.Password,
                NameVisible = user.NameVisible,
                FromDate = user.FromDate,
                ThruDate = user.ThruDate,
            });

            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Users_Create([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<User> users)
        {
            var entities = new List<User>();
            if (users != null && ModelState.IsValid)
            {
                foreach(var user in users)
                {
                    var entity = new User
                    {
                            Username = user.Username,
                            Password = user.Password,
                            NameVisible = user.NameVisible,
                            FromDate = user.FromDate,
                            ThruDate = user.ThruDate,
                    };

                    db.Users.Add(entity);
                    entities.Add(entity);
                }
                db.SaveChanges();
            }

            return Json(entities.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Users_Update([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<User> users)
        {
            var entities = new List<User>();
            if (users != null && ModelState.IsValid)
            {
                foreach(var user in users)
                {
                    var entity = new User
                    {
                        ID = user.ID,
                        Username = user.Username,
                        Password = user.Password,
                        NameVisible = user.NameVisible,
                        FromDate = user.FromDate,
                        ThruDate = user.ThruDate,
                    };

                    entities.Add(entity);
                    db.Users.Attach(entity);
                    db.Entry(entity).State = EntityState.Modified;                        
                }
                db.SaveChanges();
            }

            return Json(entities.ToDataSourceResult(request, ModelState));
        } 

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Users_Destroy([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<User> users)
        {
            var entities = new List<User>();
            if (ModelState.IsValid)
            {
                foreach(var user in users)
                {
                    var entity = new User
                    {
                        ID = user.ID,
                        Username = user.Username,
                        Password = user.Password,
                        NameVisible = user.NameVisible,
                        FromDate = user.FromDate,
                        ThruDate = user.ThruDate,
                    };

                    entities.Add(entity);
                    db.Users.Attach(entity);
                    db.Users.Remove(entity);
                }
                db.SaveChanges();
            }

            return Json(entities.ToDataSourceResult(request, ModelState));
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
