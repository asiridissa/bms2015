﻿﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using BMS.Data;

namespace BMS.Web.Controllers
{
    public class ProductController : Controller
    {
        private BMSEntities db = new BMSEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Products_Read([DataSourceRequest]DataSourceRequest request)
        {
            IQueryable<Product> products = db.Products;
            DataSourceResult result = products.ToDataSourceResult(request, product => new {
                ID = product.ID,
                ShortName = product.ShortName,
                FullName = product.FullName,
                FromDate = product.FromDate,
                ThruDate = product.ThruDate,
            });

            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Products_Create([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Product> products)
        {
            var entities = new List<Product>();
            if (products != null && ModelState.IsValid)
            {
                foreach(var product in products)
                {
                    var entity = new Product
                    {
                            ShortName = product.ShortName,
                            FullName = product.FullName,
                            FromDate = product.FromDate,
                            ThruDate = product.ThruDate,
                    };

                    db.Products.Add(entity);
                    entities.Add(entity);
                }
                db.SaveChanges();
            }

            return Json(entities.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Products_Update([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Product> products)
        {
            var entities = new List<Product>();
            if (products != null && ModelState.IsValid)
            {
                foreach(var product in products)
                {
                    var entity = new Product
                    {
                        ID = product.ID,
                        ShortName = product.ShortName,
                        FullName = product.FullName,
                        FromDate = product.FromDate,
                        ThruDate = product.ThruDate,
                    };

                    entities.Add(entity);
                    db.Products.Attach(entity);
                    db.Entry(entity).State = EntityState.Modified;                        
                }
                db.SaveChanges();
            }

            return Json(entities.ToDataSourceResult(request, ModelState));
        } 

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Products_Destroy([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Product> products)
        {
            var entities = new List<Product>();
            if (ModelState.IsValid)
            {
                foreach(var product in products)
                {
                    var entity = new Product
                    {
                        ID = product.ID,
                        ShortName = product.ShortName,
                        FullName = product.FullName,
                        FromDate = product.FromDate,
                        ThruDate = product.ThruDate,
                    };

                    entities.Add(entity);
                    db.Products.Attach(entity);
                    db.Products.Remove(entity);
                }
                db.SaveChanges();
            }

            return Json(entities.ToDataSourceResult(request, ModelState));
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetProductJson()
        {
            return Json(db.Products);
        }
    }
}
