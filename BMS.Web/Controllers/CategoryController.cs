﻿﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using BMS.Data;

namespace BMS.Web.Controllers
{
    public class CategoryController : Controller
    {
        private BMSEntities db = new BMSEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Categories_Read([DataSourceRequest]DataSourceRequest request)
        {
            IQueryable<Category> categories = db.Categories;
            DataSourceResult result = categories.ToDataSourceResult(request, category => new {
                ID = category.ID,
                ShortName = category.ShortName,
                FullName = category.FullName,
                FromDate = category.FromDate,
                ThruDate = category.ThruDate,
            });

            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Categories_Create([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Category> categories)
        {
            var entities = new List<Category>();
            if (categories != null && ModelState.IsValid)
            {
                foreach(var category in categories)
                {
                    var entity = new Category
                    {
                            ShortName = category.ShortName,
                            FullName = category.FullName,
                            FromDate = category.FromDate,
                            ThruDate = category.ThruDate,
                    };

                    db.Categories.Add(entity);
                    entities.Add(entity);
                }
                db.SaveChanges();
            }

            return Json(entities.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Categories_Update([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Category> categories)
        {
            var entities = new List<Category>();
            if (categories != null && ModelState.IsValid)
            {
                foreach(var category in categories)
                {
                    var entity = new Category
                    {
                        ID = category.ID,
                        ShortName = category.ShortName,
                        FullName = category.FullName,
                        FromDate = category.FromDate,
                        ThruDate = category.ThruDate,
                    };

                    entities.Add(entity);
                    db.Categories.Attach(entity);
                    db.Entry(entity).State = EntityState.Modified;                        
                }
                db.SaveChanges();
            }

            return Json(entities.ToDataSourceResult(request, ModelState));
        } 

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Categories_Destroy([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Category> categories)
        {
            var entities = new List<Category>();
            if (ModelState.IsValid)
            {
                foreach(var category in categories)
                {
                    var entity = new Category
                    {
                        ID = category.ID,
                        ShortName = category.ShortName,
                        FullName = category.FullName,
                        FromDate = category.FromDate,
                        ThruDate = category.ThruDate,
                    };

                    entities.Add(entity);
                    db.Categories.Attach(entity);
                    db.Categories.Remove(entity);
                }
                db.SaveChanges();
            }

            return Json(entities.ToDataSourceResult(request, ModelState));
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetCategoryJson()
        {
            return Json(db.Categories);
        }
    }
}
