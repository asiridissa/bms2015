﻿﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using BMS.Data;

namespace BMS.Web.Controllers
{
    public class VehicleController : Controller
    {
        private BMSEntities db = new BMSEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Vehicles_Read([DataSourceRequest]DataSourceRequest request)
        {
            IQueryable<Vehicle> vehicles = db.Vehicles;
            DataSourceResult result = vehicles.ToDataSourceResult(request, vehicle => new {
                ID = vehicle.ID,
                No = vehicle.No,
                FromDate = vehicle.FromDate,
                ThruDate = vehicle.ThruDate,
            });

            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Vehicles_Create([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Vehicle> vehicles)
        {
            var entities = new List<Vehicle>();
            if (vehicles != null && ModelState.IsValid)
            {
                foreach(var vehicle in vehicles)
                {
                    var entity = new Vehicle
                    {
                            No = vehicle.No,
                            FromDate = vehicle.FromDate,
                            ThruDate = vehicle.ThruDate,
                    };

                    db.Vehicles.Add(entity);
                    entities.Add(entity);
                }
                db.SaveChanges();
            }

            return Json(entities.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Vehicles_Update([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Vehicle> vehicles)
        {
            var entities = new List<Vehicle>();
            if (vehicles != null && ModelState.IsValid)
            {
                foreach(var vehicle in vehicles)
                {
                    var entity = new Vehicle
                    {
                        ID = vehicle.ID,
                        No = vehicle.No,
                        FromDate = vehicle.FromDate,
                        ThruDate = vehicle.ThruDate,
                    };

                    entities.Add(entity);
                    db.Vehicles.Attach(entity);
                    db.Entry(entity).State = EntityState.Modified;                        
                }
                db.SaveChanges();
            }

            return Json(entities.ToDataSourceResult(request, ModelState));
        } 

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Vehicles_Destroy([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Vehicle> vehicles)
        {
            var entities = new List<Vehicle>();
            if (ModelState.IsValid)
            {
                foreach(var vehicle in vehicles)
                {
                    var entity = new Vehicle
                    {
                        ID = vehicle.ID,
                        No = vehicle.No,
                        FromDate = vehicle.FromDate,
                        ThruDate = vehicle.ThruDate,
                    };

                    entities.Add(entity);
                    db.Vehicles.Attach(entity);
                    db.Vehicles.Remove(entity);
                }
                db.SaveChanges();
            }

            return Json(entities.ToDataSourceResult(request, ModelState));
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
