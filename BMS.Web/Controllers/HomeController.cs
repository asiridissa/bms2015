﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using BMS.Data;
using BMS.Service;

namespace BMS.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Welcome to ASP.NET MVC!";

            return View();
        }

        public ActionResult App()
        {
            return PartialView();
        }

        public ActionResult TopNavigationbar()
        {
            return PartialView();
        }


        public ActionResult Sidebar()
        {
            return PartialView();
        }

        public ActionResult OffSidebar()
        {
            return PartialView();
        }

        public ActionResult Footer()
        {
            return PartialView();
        }

        public ActionResult Dashboard()
        {
            return PartialView();
        }

        public JsonResult SideMenu()
        {
            return Json(new Service.SideMenuSvc().GetSideMenu());
        }

        public FileResult SideMenuJsonFile()
        {
            var file = System.IO.File.ReadAllBytes(Server.MapPath("~") + "/server/sidebar-menu.json");
            return File(file,"application/json");
        }
    }
}