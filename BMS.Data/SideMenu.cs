//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BMS.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class SideMenu
    {
        public int Id { get; set; }
        public string text { get; set; }
        public string translate { get; set; }
        public Nullable<bool> heading { get; set; }
        public string sref { get; set; }
        public string icon { get; set; }
        public string alert { get; set; }
        public Nullable<int> SortId { get; set; }
        public Nullable<int> ParentId { get; set; }
    }
}
