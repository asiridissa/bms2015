﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;

namespace BMS.Service.Controllers
{
    public class TestController : ApiController
    {
        public string Test(string str = "A")
        {
            return str;
        }

        public JsonResult<string> TestJsonResult(string str)
        {
            return Json(str);
        } 
    }
}
