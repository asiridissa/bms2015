﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BMS.BL;
using BMS.Data;
using BMS.Service;

namespace BMS.Service.Controllers
{
    public class SideMenuAPIController : ApiController
    {
        public List<Data.SideMenu> GetSideMenu()
        {
           return new SideMenuMgt().GetSideMenu();
        } 
    }
}
