﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMS.BL;

namespace BMS.Service
{
    public class AreaSvc
    {
        public List<Data.Area> GetSideMenu()
        {
            return new AreaMgt().GetArea();
        }
    }
}
