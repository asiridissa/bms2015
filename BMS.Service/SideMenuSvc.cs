﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BMS.BL;

namespace BMS.Service
{
    public class SideMenuSvc
    {
        public List<Data.SideMenu> GetSideMenu()
        {
            return new SideMenuMgt().GetSideMenu();
        }
    }
}
